<?php

class about_usController extends adminController {
	
	function __construct(){
		parent::__construct("About_Us", "about_us");
	}
  	
	function index(Array $params = []){
    $this->_viewData->hasCreateBtn = true;    

    parent::index($params);
  }  	
}