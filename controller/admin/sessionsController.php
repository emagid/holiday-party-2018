<?php

class sessionsController extends adminController {
	
	function __construct(){
		parent::__construct("Session", "sessions");
	}

	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		
        $params['orderBy'] = 'date_time ASC';

		parent::index($params);
	}

	function update(Array $arr = []){

		parent::update($arr);
	}

	function update_post(Array $arr = []){

		parent::update_post($arr);
	}

	function import_post(Array $arr = []) {
		if($_FILES['file']['error'] > 0){
            $n = new \Notification\ErrorHandler('File is invalid');
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL."sessions/");
        } else {
        	$mFile = fopen($_FILES["file"]["tmp_name"],'r');
            $header    = fgetcsv($mFile);

            while($row = fgetcsv(mFile)) {

            }
        }
		// redirect(ADMIN_URL.'/sessions');
	}

}