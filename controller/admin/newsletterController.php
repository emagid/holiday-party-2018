<?php

class newsletterController extends adminController {
	
	function __construct(){
		parent::__construct("Newsletter");
	}
  	
	function index(Array $params = []){
		$this->_pageSize = 20;
		$this->_viewData->page_title = "Newsletter Sign up";
		$params['queryOptions'] = ['orderBy'=>'newsletter.email', 'where'=>['is_subscribe'=>'1']];
		parent::index($params);
	}
  	
}