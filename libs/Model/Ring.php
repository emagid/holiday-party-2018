<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/17/15
 * Time: 1:52 PM
 */

namespace Model;

class Ring extends \Emagid\Core\Model {
    public static $tablename = "ring";

    public static $fields = [
        "name",
        "default_metal",
        'price',
        'default_color',
        'dwt',
        'semi_mount_weight',
        'stone_breakdown',
        'center_stone_weight',
        'center_stone_shape',
        'total_stones',
        'semi_mount_diamond_weight',
        'diamond_color',
        'diamond_quality',
        'cost_per_dwt',
        'fourteenkt_gold_price',
        'eighteenkt_gold_price',
        'platinum_price',
        'website_fourteenkt_gold_price',
        'website_eighteenkt_gold_price',
        'website_platinum_price',
        'slug',
        'price',
        'min_carat',
        'max_carat',
        'shape',
        'video_link',
        'colors',
        'metals',
        'setting_labor',
        'center_stone_setting_labor',
        'polish_labor',
        'head_to_add',
        'melee_cost',
        'shipping_from_cast',
        'shipping_to_customer',
        'total_14kt_cost',
        'total_18kt_cost',
        'total_plat_cost',
        'metal_colors',
        'setting_style',
        'options',
        'alias',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'description',
        'featured',
        'discount'
    ];

    static $relationships = [
        [
        'name' => 'product_category',
        'class_name' => '\Model\Product_Category',
        'local' => 'id',
        'remote' => 'product_map_id',
        'remote_related' => 'category_id',
        'relationship_type' => 'many'
        ],
    ];

    public static function defaultMetalColors(){
        return '{"14kt_gold":{"White":0,"Yellow":0,"Rose":0},"18kt_gold":{"White":0,"Yellow":0,"Rose":0},"Platinum":{"White":0,"Yellow":0,"Rose":0}}';
    }

    public static $searchFields = ['name'];

    public static function searchMetaData()
    {
        $data = [];
        $objects = self::getList();
        foreach($objects as $object){
            $fieldData = [];
            foreach(self::$searchFields as $field){
                $fieldData[] = $object->$field;
            }

            $data[] = ['fields' => $fieldData, 'object' => $object];
        }

        return $data;
    }

    public function generateSearchResult()
    {
        return ['name' => $this->name, 'image' => $this->featuredImage(), 'link' => "/rings/{$this->slug}"];
    }

    public function getImageFolderName()
    {
        $slugArray = explode('-', $this->slug);
        $slug = $slugArray[0];
        $folderName = "{$slug}-E";
        if(file_exists(UPLOAD_PATH."images/$folderName")){
            return $folderName;
        } elseif(file_exists(UPLOAD_PATH."images/{$slug}")) {
            return $slug;
        } else {
            return false;
        }

    }

    public function getVideoFolderName()
    {
        $folderName = "{$this->slug}-E";
        if(file_exists(UPLOAD_PATH."videos/$folderName")){
            return $folderName;
        } elseif(file_exists(UPLOAD_PATH."videos/{$this->slug}")) {
            return $this->slug;
        } else {
            return false;
        }
    }

    public function videoLink()
    {
        if($folderName = $this->getVideoFolderName()){
            $videoArray = [];
            $videos = glob(UPLOAD_PATH."videos/$folderName/" . "*.mp4");
            foreach($videos as $video){
                $video = str_replace(UPLOAD_PATH, UPLOAD_URL, $video);
                $videoArray[] = $video;
            }

            return $videoArray;
        } elseif($this->video_link){
            return [UPLOAD_URL.'rings/'.$this->video_link];
        } else {
            return null;
        }
    }

    public function featuredImage()
    {
        if($foldName = $this->getImageFolderName()){
            return UPLOAD_URL."images/$foldName/$foldName.jpg";
        } else {
            $productImage = new Product_Image();
            $productImage = $productImage->getProductImage($this->id, 'ring');
            if(!empty($productImage)){
                return UPLOAD_URL."products/".$productImage[0]->image;
            }

            return FRONT_ASSETS."img/ring5.png";
        }
    }

    public function getImages()
    {
        if($foldName = $this->getImageFolderName()){
            $images = glob(UPLOAD_PATH."images/$foldName/" . "*.jpg");
            $imageArray = [];
            foreach($images as $image){
                $image = str_replace(UPLOAD_PATH, UPLOAD_URL, $image);
                $imageArray[] = $image;
            }
        } else {
            $productImage = new Product_Image();
            $productImage = $productImage->getProductImage($this->id, 'ring');
            $imageArray = [];
            if($productImage){
                // get rid of featured image
                foreach($productImage as $image){
                    $imageArray[] = UPLOAD_URL."products/".$image->image;
                }
            }
        }

        return $imageArray;
    }

    public function comboRing(){
        if(isset($_SESSION['product'])){
            return true;
        } else {
            return false;
        }
    }

    public function getPrice($selectMetal = '14KT GOLD', $stone = null, $original = false)
    {
        if(!$stone){
            $stone = $this->getDefaultStone();
        }

        $price = $this->getPrices($stone);

        $finalPrice = $price['14kt_gold'];

        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                break;
            case '18KT GOLD':
                $finalPrice = $price['18kt_gold'];
                break;
            case 'PLATINUM':
                $finalPrice = $price['Platinum'];
                break;
        }

        if($original){
            return $finalPrice;
        }

        if($this->featured && $this->discount){
            $finalPrice = $finalPrice * (100 - floatval($this->discount)) / 100;
            $finalPrice = floatval($finalPrice);
        }

        return $finalPrice;
    }

    public function getPriceDiff($selectMetal, $stone)
    {
        $price = $this->getPrices($stone);

        $priceDiff = $price['14kt_gold'];
        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                return 0;
            case '18KT GOLD':
                $priceDiff = $this->formatPrice($price['18kt_gold']) - $this->formatPrice($price['14kt_gold']);
                break;
            case 'PLATINUM':
                $priceDiff = $this->formatPrice($price['Platinum']) - $this->formatPrice($price['14kt_gold']);
                break;
        }

        if($this->featured && $this->discount){
            $priceDiff = $priceDiff * (100 - floatval($this->discount)) / 100;
            $priceDiff = number_format($priceDiff, 2);
            $priceDiff = floatval($priceDiff);
        }

        return $priceDiff;
    }

    public function getOriginalPrice()
    {
        return $this->getPrice('14KT GOLD', null, true);
    }

    private function formatPrice($string)
    {
        $string = str_replace(['$', ','], '', $string);
        $string = trim($string);
        return floatval($string);
    }

    public function getMap()
    {
        $map = new Product_Map();
        return $map->getMap($this->id, 'Ring');
    }

    public static function getColors(){
        return ['White','Yellow','Rose'];
    }

    public static function getMetals(){
        return ['14kt gold', '18kt gold', 'Platinum'];
    }

    public function getUrl()
    {
        return "/rings/{$this->slug}";
    }

    public function getWebsiteMetalPrice($metal){
        switch($metal){
            case '14kt gold':
                return preg_replace("([^0-9\\.])", "", $this->website_fourteenkt_gold_price);break;
            case '18kt gold':
                return preg_replace("([^0-9\\.])", "", $this->website_eighteenkt_gold_price);break;
            case 'Platinum':
                return preg_replace("([^0-9\\.])", "", $this->website_platinum_price);break;
        }
    }

    public static function printRingSizeOptions($selected,$tag = 'option')
    {
        $html = '';
        for($i=4; $i<=9; $i += .25) {
            if($i == $selected){
                $html .= "<$tag selected>$i</$tag>";
            } else {
                $html .= "<$tag>$i</$tag>";
            }
        }

        return $html;
    }

    public function getOptions()
    {
        return json_decode($this->options, true);
    }

    public function getDefaultOption($getStone = 0)
    {
        $data = $this->getOptions();
        $keys = array_keys($data);

        return $data[$keys[$getStone]];
    }

    /**
     * @param $stone, can be string or int
     * @return mixed
     */
    public function getPrices($stone)
    {
        $options = $this->getOptions();
        if(is_numeric($stone)){
            $key = array_keys($options)[$stone];
            return $options[$key]['price'];
        } else {
            return $options[$stone]['price'];
        }
    }

    public function getDefaultStone($getStone = 0)
    {
        $data = $this->getOptions();
        $keys = array_keys($data);
        return $keys[$getStone];
    }

    public static function randomizer($limit = 10){
        $items = self::getList();
        shuffle($items);
        return array_slice($items,0,$limit);
    }
}