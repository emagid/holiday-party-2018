<div id='kiosk4' class='kiosk_check'></div>


<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk4"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='title_holder purp'>
	<h2>How important are narrative and perception change to uplifting black men and boys?</h2>
</section>

<section class='text_full'>
	<ol>
		<li class='active_ans'><i class="fas fa-check"></i><span>Very Important</span></li>
		<li><i class="fas fa-check"></i><span>Somewhat Important</span></li>
		<li><i class="fas fa-check"></i><span>Not That Important</span></li>
		<li><i class="fas fa-check"></i><span>No Opinion</span></li>
	</ol>
</section>

<div class='popup'>
	<section>
		<h3>THANK YOU!</h3>
	</section>
</div>

<button id='survey' class='btn purp'>SUBMIT<i class="fas fa-caret-right"></i></button>


<script type="text/javascript">
	$('li').click(function(){
		$('li').removeClass('active_ans');
		$(this).addClass('active_ans');
	});


	$('#survey').click(function(){
		$('.popup').slideDown();
		var answer = $('.active_ans span').html();
		// console.log(answer);
		// POST ANSWER IN BACKEND
		$.post("/home/submit_survey",{answer:answer}, function(data){
			
		})
		setTimeout(function(){
			// once answer has been posted reroute
			$('.white').fadeIn();
			setTimeout(function(){
				window.location = "/home/kiosk4";
			}, 500)
		}, 3000);
	});
</script>