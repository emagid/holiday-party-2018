<div id='kiosk4' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk4"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='title_holder purp'>
	<h2>NARRATIVE CHANGE + STORY-TELLING</h2>
</section>

<section class='text_full'>
	<p>Over the years, CBMA has curated and sponsored events and conversations featuring influential artists, media makers, thought leaders and more to explore narrative change, storytelling, and transforming perceptions of Black men and boys in the media and popular culture. Check out some of the most notable people we’ve partnered and collaborated with.</p>
</section>

<a href="/home/influencers"><button class='btn purp'>SEE OUR STORIES<i class="fas fa-caret-right"></i></button></a>


