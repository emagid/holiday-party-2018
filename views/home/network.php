<div id='kiosk6' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk6"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='title_holder purp'>
	<h2>CBMA NETWORK</h2>
</section>

<section class='text_full'>
	<p>CBMA is a growing national network of leaders and organizations working to advance Black Male Achievement across the country, including 5,700+ members and 2,765+ organizations. According to our June 2018 network survey, CBMA has engaged both large and small organizations in its network, with organizational budgets ranging from $1,000 to $45 million per year.
</p>
</section>
