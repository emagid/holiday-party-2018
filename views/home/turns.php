<div id='kiosk1' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk1"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='text_full'>
	<p>CBMA is excited to host our 10th Anniversary “Black Male Re-imagined” Gala and Fundraiser to celebrate a decade of movement and field building to uplift Black men and boys! Learn more about our honorees George Soros, Tonya Allen and John W. Rogers, Jr. and our host Hill Harper.</p>
</section>
<h3 class='ten'>10</h3>

<button class='btn'>LEARN MORE<i class="fas fa-caret-right"></i></button>

<img id='close' class='black_close' src="<?= FRONT_ASSETS ?>img/x_dark.png" style='color: white;'>
<section class='slider black_slide'>
	<div class='slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk1_img2.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote.png">
			</div>
			<h4>HONOREE: GEORGE SOROS -- FOUNDER & CHAIR, SOROS FUND MANAGEMENT LLC AND THE OPEN SOCIETY FOUNDATIONS</h4>
		</div>
		<div class='quotes'>
			<p class='quote'>"</p>
			<p>Through the Open Society Foundations, I have worked for many years to address the systemic injustices that prevent so many African American men from achieving their full potential. We established a Campaign for Black Male Achievement to work in conjunction with other initiatives to increase the number and effectiveness of education programs for young men of color, in order to open pathways to challenging, fulfilling jobs and to strengthen minority families devastated by the systemic and unjust removal of so many men—and women— from their midst.”</p>
			<p>--"Where Do We Go From Here: Philanthropic Support for Black Men and Boys" </p>
		</div>
	</div>

	<div class='slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk1_img4.png');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote.png">
			</div>
			<h4>HONOREE: TONYA ALLEN -- PRESIDENT & CHIEF EXECUTIVE OFFICER, SKILLMAN FOUNDATION</h4>
		</div>
		<div class='quotes'>
			<p class='quote'>"</p>
			<p>“Nationally, because of concerted efforts like the Campaign for Black Achievement, I’ve seen scores of foundations and corporations commit to work toward the same goals. This alignment of actions has the potential to address disparities affecting young men of color in an unprecedented way.”</p>
			<p>Quantifying Hope: Philanthropic Support for Black Men and Boys</p>
		</div>
	</div>


	<div class='slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk1_img5.png');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote.png">
			</div>
			<h4>HONOREE: JOHN W. ROGERS, JR. -- CHAIRMAN & CHIEF EXECUTIVE OFFICER, ARIEL INVESTMENTS</h4>
		</div>
		<div class='quotes'>
			<p class='quote'>"</p>
			<p>Everybody here is a leader, everybody here is involved with boards and leadership in their organizations....if we can all come together that we’re going to be looking out for each other and building an opportunity for all of us to grow together, then we’ll be able to impact our society and some of these issues that we care so desperately about will start to diminish.” 
				<p>--Black Male Achievement Innovation & Impact Forum </p>
		</div>
	</div>
	
	<div class='slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk1_img3.png');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote.png">
			</div>
			<h4>HOST: HILL HARPER -- HUMANITARIAN, ACTOR AND AUTHOR</h4>
		</div>
		<div class='quotes'>
			<p class='quote'>"</p>
			<p>Change happens from the bottom up - all of us as individuals deciding that we will and we do have an impact.
			<p> --CNN Interview</p>
		</div>
	</div>
</section>

<script type="text/javascript">

	$('.slider').slick({
	    arrows: true,
	    slidesToShow: 1,
	    autoplay: true,
	    focusOnSelect: false,
	    autoplaySpeed: 3000
	  });

	$('.action').click(function(){
		if ( $(this).hasClass('open') ) {
			$('button').show();
			$(this).parents('.slide').children('.quotes').slideUp();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote.png');
			$(this).removeClass('open');
			$('.slider').slick({
			    autoplay: true,
			    focusOnSelect: false,
			    autoplaySpeed: 3000
			  });
			$('.black_close').fadeOut();
		}else {
			$('button').hide();
			$(this).parents('.slide').children('.quotes').slideDown();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/x.png');
			$(this).addClass('open');
			$('.black_close').fadeIn();
		}
	});

	$('#close').click(function(){
		$('button').show();
		$('.quotes').slideUp();
		$('.info h4').fadeIn();
		$('#close').fadeOut();
		$('.action').removeClass('open');
		$('.action').children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote.png');
	});
</script>