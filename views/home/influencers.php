<div id='kiosk4' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk4"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<img id='close' src="<?= FRONT_ASSETS ?>img/x_dark.png" style='color: white;'>
<section class='slider'>
	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk4_img2.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>TA-NEHISI COATES, Journalist and Author</h4>
		</div>
		<div class='quotes'>
			<h4>TA-NEHISI COATES, Journalist and Author</h4>
			<p>“I think a lot of times when people talk about Black folks and young Black boys in particular, there’s great focus on their attitudes and how they conduct themselves out in the world, and there’s great focus on the notion of them being scary and hard and acting a certain way, but there’s very little attempt to understand what’s actually going on. I was in a great position to explain that because I was one of those boys. One of the big things that’s behind that, and has always been behind that, is fear.”</p>
			<p>--CBMA Monthly Network Call, July 2015</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk4_img3.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>RASHAD ROBINSON, Executive Director, Color of Change</h4>
		</div>
		<div class='quotes'>
			<h4>RASHAD ROBINSON, Executive Director, Color of Change</h4>
			<p>“We at Color of Change are absolutely inspired by so much of the work to elevate and amplify the voices of Black people in general, and work to push back against so much of the challenges we see in terms of Black men in the media. Some of the early support that we were able to get from CBMA helped me and my organization transfer our vision into actually something real; the ideas that we had on paper, that know-how that we had, into the actual work. And so having that support, having that partnership and being part of the table of so many leaders around the country doing this work collaborating and supporting one another has been key.”</p>
			<p>--CBMA Monthly Network Call, September 2015</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk4_img4.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>SUSAN L. TAYLOR, Founder/CEO, National CARES Mentoring Movement, Editor-In-Chief Emerita of Essence Magazine</h4>
		</div>
		<div class='quotes'>
			<h4>SUSAN L. TAYLOR, Founder/CEO, National Cares Mentoring Movement, Editor-In-Chief Emerita of Essence Magazine</h4>
			<p>"We believe we all have a responsibility to answer our children's cry for our help. Mentoring is all about caring. It's caring enough to commit just one hour a week to advise and help guide a vulnerable young person. None of the forces claiming our children's lives are more powerful than our commitment and love. We are the solution.”</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk4_img5.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>JESSE WILLIAMS, Actor & Activist & Executive Producer of “Question Bridge: Black Males” </h4>
		</div>
		<div class='quotes'>
			<h4>JESSE WILLIAMS, Actor & Activist & Executive Producer of “Question Bridge: Black Males” </h4>
			<p>“Thanks to CBMA, Rashid (Shabazz) and Aperture for your relentless creativity and support for movements forward for our folks...Being able to use media to be informative and purposeful and create a vehicle for us to not wait for other people to represent the Black voice, Black perspective and experience anymore and find a way to cut out the middleman and go directly to us, for us and by us. That’s why I got involved.” </p>
			<p>--CBMA Monthly Network Call, September 2015</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk4_img6.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>Harry Belafonte, Humanitarian, Activist and Entertainer</h4>
		</div>
		<div class='quotes'>
			<h4>Harry Belafonte, Humanitarian, Activist and Entertainer</h4>
			<p>"I strongly believe that America has lost its moral compass. I think that Muhammad ALi and many other leaders have left us with examples of choices we can make. I would hope that this gathering, and all of you who are here, in this honor that you bestow on me, would keep in mind that our struggle is far, far from over. We have a long way to go.”</p>
			<p>--Excerpt from Belafonte’s Lifetime Achievement Award acceptance speech at Rumble Young Man, Rumble VI in 2016.</p>
		</div>
	</div>
</section>

<script type="text/javascript">

	$('.slider').slick({
	    arrows: true,
	    slidesToShow: 1,
	    autoplay: true,
	    focusOnSelect: false,
	    autoplaySpeed: 3000
	  });

	$('.action').click(function(){
		if ( $(this).hasClass('open') ) {
			$('button').show();
			$(this).parents('.slide').children('.quotes').slideUp();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
			$(this).removeClass('open');
			$('.info h4').fadeIn();
			$('#close').fadeOut();
			
		}else {
			$('button').hide();
			$(this).parents('.slide').children('.quotes').slideDown();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/x_dark.png');
			$(this).addClass('open');
			$('.info h4').fadeOut();
			$('#close').fadeIn();
		}
	});

	$('#close').click(function(){
		$('button').show();
		$('.quotes').slideUp();
		$('.info h4').fadeIn();
		$('#close').fadeOut();
		$('.action').removeClass('open');
		$('.action').children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
	});
</script>