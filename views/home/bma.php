<div id='kiosk3' class='kiosk_check'></div>


<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk3"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='title_holder gold'>
	<h2>BUILDING THE BMA FIELD</h2>
</section>

<section class='text_full'>
	<p>For the past 10 years, CBMA has convened and hosted events aimed at building the Black Male Achievement field and movement, creating community among our network of leaders, strengthening leadership capacity, and broadening awareness. Check out highlights from our most noteworthy events.</p>
</section>

<a href="/home/events"><button class='btn gold'>check out the highlights<i class="fas fa-caret-right"></i></button></a>
