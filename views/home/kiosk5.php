<div id='kiosk5' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<section class='link_holders normal'>
	<div class='links'>
		<a href="/home/future">building the future</a>
		<a href="/home/testimonials">TESTIMONIALS</a>
		<a href="/home/decade">The Next Decade?</a>
	</div>
</section>

<section class='background_text' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk5_img1.jpg');">
</section>