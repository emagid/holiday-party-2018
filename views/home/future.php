<div id='kiosk5' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk5"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='title_holder purp'>
	<h2 style='font-size: 75px;'>BUILDING THE FUTURE</h2>
</section>

<section class='text_full'>
	<p>In the past decade, CBMA has spurred lots of activity, promise and increased philanthropic and public investment to build the field of Black Male Achievement. As CBMA looks aheads towards and beyond the next 10 years, we are committed to ensuring that the investments we make take root in communities and are sustained over a generation. In so doing, we are guided by risk, urgency and momentum: the risk and urgency of failing to act propel us forward to build on the momentum of the last ten years.</p>
</section>
