<div class="productInfo_pagewrapper">
	<h2><circle><span>i</span></circle>Lab-Grown Diamonds</h2>
	<div class="contentRow contentRow_of_3 aboutIntroRow">
		<div class="col">
			<div class="media productInfoIntro"></div>
		</div>
		<div class="col aboutIntro">
			<h4>About Lab-Grown Diamonds</h4>
			<p>Lab grown diamonds are physically, optically, and chemically indistinguishable from mined diamonds. They provide exceptional value and typically cost 30-40% less than mined stones of the same size, cut and quality.</p>
			<a class="productInfoBtn" href="<?=SITE_URL?>contact">Shop Now</a>
		</div>
		<div class="col benefits">
			<h4>The Benefits of Grown Diamonds</h4>
			<p><b>Local:</b> Diamond Lab Grown Diamonds are grown here in the USA and help support our economy. By knowing the origin of your diamond you can be proud to say where it came from.</p>
			<p><b>Ethical Appeal:</b> Our diamonds are extremely eco-friendly compared to the mining process which takes one ton of earth to be removed to extract just one carat of a gem quality diamond. Our diamonds are also 100% conflict free.</p>
		</div>
	</div>
	<div class="contentRow contentRow_of_2 three_blocks_info">
		<div class="col">
			<div class="col">
				<h4>Polishing and Certification process</h4>
				<p>Our diamonds are cut the same way you would cut a mined diamond, on a diamond wheel. Once our diamonds are shaped and polished, every diamond passes through our highly trained inspectors to make sure every stone is cut the right way. All of our diamonds are hand selected by Gemological Institute of America graduates to ensure the highest quality of diamonds are set in our jewelry.</p>
			</div>
			<div class="col">
				<h4>Jewelry</h4>
				<p>All of our jewelry is made and assembled here in America from recycled metals. Luckily precious metals can be recycled without losing any of their important properties. By using recycled metals we could slowly prevent the need to dig for more.</p>
			</div>
		</div>
		<div class="col">
			<h4>Five Item or More</h4>
			<p>if you would like to order 5 items or more from our store, please email us at <a href="mailto:info@diamondlabgrown.com">info@diamondlabgrown.com</a></p>
		</div>
	</div>
	<div>
		
		<div class="contentRow contentRow_60_40">
			<div class="col">
				<h2>How our <span>Lab-Grown Diamonds</span> <b>are Grown</b></h2>
				<p>At Diamond Lab Grown we use Chemical Vapor Deposition (CVD) to grow beautiful gem quality diamonds in only a few weeks. This process starts with a tiny “diamond seed” which is placed in a sealed tight gas chamber and filled with methane and hydrogen.</p>
				<p>Layer by layer, diamond is grown and we stop the process when it reaches maximum capacity. With over 40 patent on the process we are able to create high color diamonds without any treatments.</p>
			</div>
			<div class="col">
				<div class="media productInfoCall">
					<a href="<?=SITE_URL?>contact"></a>
				</div>
			</div>
		</div>
	</div>
</div>