<div class = "global_footer blur_on_overlay">
	<div class = "footer_content_wrapper">
		<div class = "row">
			<div class = "left_footer_section">
				<ul class = "footer_links">
					<li class = "footer_link_list_title">
						<p>About</p>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>about">Our Company</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>aboutlabgrowndiamonds">Lab-Grown Diamonds</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>contact">Contact Us</a>
					</li>
				</ul>
				<ul class = "footer_links">
					<li class = "footer_link_list_title">
						<p>At your Service</p>
					</li>
					<?$pages = ['shipping','returns','faq','warranty'];
					foreach($model->pages as $page){
						if(in_array($page->slug,$pages)){?>
							<li class = "footer_list_link">
								<a href="<?=SITE_URL.'page/'.$page->slug?>"><?=$page->title?></a>
							</li>
					<?}
					}?>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>expertise/the4cs">The 4Cs</a>
					</li>
				</ul>
				<ul class = "footer_links">
					<li class = "footer_link_list_title">
						<p>Explore</p>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>contact">Find a Diamond</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>browse/rings">Engagement Rings</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>browse/weddingbands">Wedding Bands</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>browse/jewelry">Browse Jewelry</a>
					</li>
				</ul>
			</div>
			<div class = "right_footer_section">
	            <div class="footerLinksGrouping">
	                <h6 class="sackers">Follow Us</h6>
	                <div class="row footerSocialIcons">
	                    <div class="col">
	                        <a>
	                            <div class="media" href="mailto:info@theluggagecollection.com" style="background-image:url('<?=FRONT_ASSETS?>img/mailCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                    <div class="col">
	                        <a>
	                            <div class="media" style="background-image:url('<?=FRONT_ASSETS?>img/twitterCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                    <div class="col">
	                        <a>
	                            <div class="media" style="background-image:url('<?=FRONT_ASSETS?>img/facebookCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                    <div class="col">
	                        <a>
	                            <div class="media" style="background-image:url('<?=FRONT_ASSETS?>img/instaCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                </div>
	            </div>			
				<h1>Sign up for our Newsletter</h1>
				<div class = "newsletter_section">
					<div>
						 
							<input type = "email" name="email" class="form-control" id="email_nwsl" placeholder = "Your email">
							<input type = "submit" value = "Submit" class = "mercury" id="submit">
							<script>
						  $('body').on('click', '#submit', function() {
                                
                                var email = $("#email_nwsl").val();

                                $.ajax({
                                    type: "POST",
                                    url: "/newsletter/add",
                                    data: {
                                        email: email
                                    },
                                    success: function(data) {
                                    	alert("Thanks for signing up to receive our newsletter!");
                                    }
                                });

                            });
</script>


					</div>
				</div>
				<div class="alert alert-success" style="display:none;">
					<strong>Thank You! </strong>Your are now subscribed to our newsletter.
				</div>


<!-- 				<div class = "social_section">
					<p>Follow Us</p>
					<div class = "social_links_wrapper">
						<a>
							<icon></icon>
						</a>
						<a>
							<icon></icon>
						</a>
						<a>
							<icon></icon>
						</a>
						<a>
							<icon></icon>
						</a>
					</div>
				</div> -->
			</div>
		</div>
		<div class = "row">
			<div class = "horizontal_separator"></div>
		</div>
		<div class = "row footer_bottom_row">
			<div class = "footer_bottom_row_left">
				<p>Diamond Lab Grown</p>
				<a href="<?=SITE_URL.'page/privacy-policy'?>">Privacy Policy</a>
				<a href="<?=SITE_URL.'page/terms-and-conditions'?>">Terms and Conditions</a>
			</div>
		</div>
	</div>
</div>