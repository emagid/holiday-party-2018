$postUrl = "postpuzzle.php";
$eventFull = '';
function exitHandler() {
    var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
    $eventFull = state ? 'FullscreenOn' : 'FullscreenOff';
    var event = state ? 'FullscreenOn' : 'FullscreenOff';
        console.log('Event: ' + event);
        //alert('Event: ' + event);
    if (event == 'FullscreenOff') {
        //debugger;
        $wHeight=505;
        $('.puzzle-btn>img').css('height', $wHeight);
        $('.puzzle-btn').css('height', $wHeight);
        $(window).on( "orientationchange, resize", function( event ) {
            //$wHeight = ($(window).height() / 3) + 5;
            $wHeight=505;
            $('.puzzle-btn>img').css('height', $wHeight);
            $('.puzzle-btn').css('height', $wHeight);
        })
    }else{
        //debugger;
       $wHeight=540;
        $('.puzzle-btn>img').css('height', $wHeight);
        $('.puzzle-btn').css('height', $wHeight);
        $(window).on( "orientationchange, resize", function( event ) {
            //$wHeight = ($(window).height() / 3) + 5;
            $wHeight=540;
            $('.puzzle-btn>img').css('height', $wHeight);
            $('.puzzle-btn').css('height', $wHeight);
        }) 
    }
} 
function backToLogin() {
    //$('#puzzle').addClass('hide');
    //showLogin();
    //resetButtons();
    //resetJsonArrays();
    //clearTimeout($restartLogin);
    //location.reload();
    //setEff();
    debugger;
}
function shuffle(array, size) {
  var currentIndex = size, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
function checkAns(ansVal, qVal, jsonRitghtAns){
    var ansRightArray = JSON.parse(JSON.stringify(jsonRitghtAns));
    if(parseInt(ansRightArray[qVal]) == parseInt(ansVal)){
        return true;
    }else{
        return false;
    }
}
function htmlAnswers(allAns){
    if(typeof(allAns) != "undefined"){
        var ansArray = JSON.parse(JSON.stringify(allAns));
        $radiosAns = '';
        $(ansArray).each(function(k, v){
            $.each($.parseJSON(JSON.stringify(this)), function(i, item) {
                $radiosAns += '<div class="input-field">';
                $radiosAns +=     '<input name="ans" value="'+i+'" type="radio" id="ans'+i+'" />';
                $radiosAns +=     '<label for="ans'+i+'">'+item+'</label>';
                $radiosAns += '</div>';
            })

        });
        return $radiosAns;
    }
}
function randomAns(allAns){
    if(typeof(allAns) != "undefined"){
        var aArray = $.parseJSON(JSON.stringify(allAns));
        var arraySize = 0;
        $.each(aArray, function(i, item) {
            arraySize++;
        });
        if(arraySize > 0){
            //debugger;
            return aArray;//shuffle(aArray, arraySize);
        }else{
            return "No Answers Left";
        }
    }
        
        
}
function randomQuestion(questions, arrayNum){
    var arraySize = arrayNum.length
    if(arraySize > 0){
        var randNumber = Math.floor(Math.random()*arraySize);
        var qText = questions[arrayNum[randNumber]];
        var qPos = arrayNum[randNumber];
        return {qText:qText, qPos:qPos};
    }else{
        return "No Questions Left";
    }
        
}
function showPuzzle(){
    
    $('#puzzle').removeClass('hide');
    $('#puzzle').removeClass('zoomOut');
    $('#puzzle').addClass('zoomIn');
    $('#puzzle').addClass('animated');
    window.focus();
    document.activeElement.blur();
    return false;
}
function showAnsModal(){
    $('#questionsModal').modal('show');
    
    $('#questionsModal .body-modal').removeClass('zoomOut');
    $('#questionsModal .body-modal').addClass('zoomIn');
    $('#questionsModal .body-modal').addClass('animated');
    return false;
}
function showLogin(){
    $('#loginModal').modal('show');    
    $('#loginModal .body-modal').removeClass('zoomOut');
    $('#loginModal .body-modal').addClass('zoomIn');
    $('#loginModal .body-modal').addClass('animated');
    return false;
}
function hideAnsModal(){
    
    $('#questionsModal .zoomIn').addClass('zoomOut');
    $('#questionsModal .zoomIn').addClass('animated');
    setTimeout(function(){
        $('#questionsModal .zoomOut').addClass('body-modal');
        $('#questionsModal').modal('hide');
    }, 500);
    return false;
}
function hideLogin(){
    $('#loginModal .zoomIn').addClass('zoomOut');
    $('#loginModal .zoomIn').addClass('animated');
    setTimeout(function(){
        $('#loginModal .zoomOut').addClass('body-modal');
        $('#loginModal').modal('hide');
    }, 500);
    return false;
}
function submitTimer(){
    $arrayTimer = $timeVal;
    if($arrayTimer > 60){
        $minIn = parseInt($arrayTimer / 60);
        $sec = parseInt($arrayTimer % 60);
        if(parseInt($minIn) < 10){
            $timeToPost = '00:0' + $minIn;
        }else{
            $timeToPost = '00:' + $minIn;
            
        } 
        if(parseInt($sec) < 10){
            $timeToPost += ':0' + $sec;
        }else{
            $timeToPost += ':' + $sec;
            
        } 
    }else if($arrayTimer < 60){
        if(parseInt($arrayTimer) < 10){
            $timeToPost = '00:00:0' + $arrayTimer;
        }else{
            $timeToPost = '00:00:' +$arrayTimer;
            
        } 
    }else{
    }
    /*$.ajax({
        url: $postUrl,
        type: 'POST',
        data:{
            timerDb: true,
            idDoc: $('#userId').val(),
            timer: $timeToPost,
        },
        success: function(dSuccess){
        },
        error: function(dError){
            
        }
    })*/
}
function submitLoginDB($fName, $lName){
    $.ajax({
        url: $postUrl,
        type: 'POST',
        data:{
            loginDb: true,
            fName: $fName,
            lName: $lName,
            ipAddress: $('#ip-address').val(),
        },
        success: function(dSuccess){
            $('#userId').val(dSuccess);
        },
        error: function(dError){
            
        }
    })
}
function resetButtons(){
    //setEff();
    //$wHeight = ($(window).height() / 3) + 5;
	$wHeight=256;
    $('.flipOutY.animated').each(function(){
        $(this).attr('class','');
    })
    $('.puzzle-btn img').each(function(){
        $(this).attr('style','');
    })
    $('.puzzle-btn').each(function(){
        $(this).prop('disabled', false);
        $(this).attr('style','');
        $(this).css('height', $wHeight);
    })
    
    $('#close-modal-2').addClass('hide');
    $timeVal = 0;
}
function resetJsonArrays(){
    $jsonAns = {
        "1": [
            {"1":"<b>א.</b>  פינוי כלייתי מינימלי"},
            {"2":'<b>ב.</b> עמידות לפירוק ע"י DPP4'},
            {"3":"<b>ג.</b> זמן מחצית חיים של יממה"},
            {"4":"<b>ד.</b>  מסיסות גבוהה"},
            {"5":"<b>ה.</b>  תשובות א, ב, ד נכונות"}
        ],
        "2": [
            {"1":"<b>א.</b>  נכון"},
            {"2":"<b>ב.</b> לא נכון"}
        ],
        "3": [
            {"1":"<b>א.</b>  נכון"},
            {"2":"<b>ב.</b> לא נכון"}
        ],
        "4": [
            {"1":"<b>א.</b> תוך חודש מתחילת הטיפול"},
            {"2":"<b>ב.</b> בשבוע השני מתחילת הטיפול"},
            {"3":"<b>ג.</b> תוך יום מתחילת הטיפול"},
            {"4":"<b>ד.</b> תוך 3 חודשים מתחילת הטיפול"}
        ],
        "5": [
            {"1":"<b>א.</b>  נכון"},
            {"2":"<b>ב.</b> לא נכון"}
        ],
        "6": [
            {"1":"<b>א.</b>  נכון"},
            {"2":"<b>ב.</b> לא נכון"}
        ],
        "7": [
            {"1":"<b>א.</b> 34%"},
            {"2":"<b>ב.</b> 74%"},
            {"3":"<b>ג.</b> 68%"},
            {"4":"<b>ד.</b> 83%"}
        ],
        "8": [
            {"1":"<b>א.</b>  נכון"},
            {"2":"<b>ב.</b> לא נכון"}
        ],
        "9": [
            {"1":"<b>א.</b> מטופל עם Hba1c>7.5 וBMI>30"},
            {"2":"<b>ב.</b> מטופל עם Hba1c>9 וBMI>28-30"},
            {"3":"<b>ג.</b> מטופל עם <span dir='ltr'>7.5&#60Hba1c</span> וBMI>28-30 עם מחלת לב כלילית&#47;מחלה סרברווסקולרית&#47;מחלת כליה כרונית"},
            {"4":"<b>ד.</b> כל התשובות נכונות"}
        ],
        "10": [
            {"1":"<b>א.</b> עט אוטומטי מוכן לשימוש"},
            {"2":"<b>ב.</b> מחט נסתרת"},
            {"3":'<b>ג.</b> הכנת התמיסה ע"י ניעור העט כ- 80 פעמים'},
            {"4":"<b>ד.</b> תשובות א+ב נכונות"}
        ],
    }
    $jsonQuestions___remez = {
        "1" : "In Rainbow Cyramza arm, 49% of patients had peritoneal metastasis and 39% of patients had ascites.",
        "2" : "Cyramza and paclitaxel significantly improved OS with median of 9.6 months in comparison to 7.4 months in the placebo plus Paclitaxel arm.",
        "3" : "Cyramza and paclitaxel significantly improved PFS with a median of 4.4 months in comparison to 2.9 months in the placebo plus paclitaxel arm.",
        "4" : "ORR: A significantly greater proportion of patients achieved an objective response in the ramucirumab plus paclitaxel group than in the placebo plus paclitaxel group: 28% vs 16%, p=0.0001.",
        "5" : "DCR: A significantly greater proportion of patients also achieved disease control in the ramucirumab plus paclitaxel group than in the placebo plus paclitaxel group, 80% vs 64%, p<0.0001.",
        "6" : "Cyramza and paclitaxel showed a higher percentage of patients with improved or stable QOL/symptom scores in most categories than paclitaxel and placebo.",
        "7" : "Cyramza monotherapy is one of the 2L treatment option for the patients who do not wish or tolerate any chemotherapy.",
        "8" : "Cyramza is an established 2 nd line treatment recommended by prominent oncology association guidelines: ESMO, NCCN and Japan Gastric Cancer association.",
        "9" : "Cyramza addressed 4 positive Phase III trials across 3 different tumor indications: Gastric/GEJ, NSCLC and CRC.",
        "10" : "Cyramza addressed 4 positive Phase III trials across 3 different tumor indications: Gastric/GEJ, NSCLC and CRC.",
    }
	$jsonQuestions = {
        "1" : "אילו מן התכונות הבאות מאפיינות את טרוליסיטי?",
        "2" : "אפקט האינקרטינים מופחת בחולים עם סכרת סוג 2?",
        "3" : "הסיכון להיפוגליקמיה עם טיפול בטרוליסיטי נמוך ביותר, ולכן אין צורך במעקב יומיומי של ערכי הסוכר בדם",
        "4" : "תוך כמה זמן ניתן לראות את האפקט של טרוליסיטי על רמות הסוכר בדם (FPG   )?",
        "5" : "הוכח במטה אנליזה כי טרוליסיטי אינו מגביר שכיחות ארועים קרדיווסקולריים (MACE 4)",
        "6" : "טיפול בטרוליסיטי מתאים למטופלים עם אי ספיקת כליות קלה-בינונית (eGFR>30)",
        "7" : "במחקר שבדק העדפת מטופלים של טרוליסיטי לעומת ויקטוזה, איזה אחוז העדיפו טרוליסיטי לעומת ויקטוזה?",
        "8" : "99% מהמטופלים חושבים כי הזרקת טרוליסיטי פשוטה ומצליחים לבצע ההזרקה בכוחות עצמם",
        "9" : "לפי קריטריוני סל הבריאות, מי מהמטופלים הבאים מתאים לקבל טרוליסיטי:",
        "10" : "מה מהבאים מאפיין את העט של טרוליסיטי?",
    }
    $arrayNum = [1, 2, 3, 4, 5, 6, 7, 8, 9];
}
function stopTimeing(){
    $timeVal = 0;
    if(typeof($interval) != 'undefined')
        clearInterval($interval);
}
function startTimeing($start){
    $timeVal = $start;
    $interval = setInterval(function(){
        $timeVal++;
    }, 1000);
}
function submitLogin(){
    $('#submit-login').on('click touchstart', function(e){
        if($arrayNum.length == 0){
            resetJsonArrays();
            resetButtons();
        }
        if($('#fName').val().trim() != '' && $('#lName').val().trim() != ''){
            startTimeing(0);
            $('.show-error>label').addClass('hide');
            $('#fName').parents('.error-message').removeClass('show-error');
            $('#fName').removeClass('error');
            $('#lName').removeClass('error');
            $('#close-modal-2').removeClass('hide');
            submitLoginDB($('#fName').val(), $('#lName').val());
            $('#fName').val('');
            $('#lName').val('');
            hideLogin();
            showPuzzle();
            document.activeElement.blur();
            $("input").blur();
            var docElm = document.documentElement;
             if (docElm.webkitEnterFullScreen) {
                docElm.webkitEnterFullScreen();
            }else if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            }
            else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            }
            else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            } else if (docElm.documentElement.msRequestFullscreen) {
              docElm.documentElement.msRequestFullscreen();
            } else if (docElm.documentElement.oRequestFullscreen) {
              docElm.documentElement.oRequestFullscreen();
            }
        }else{
            if($('#fName').val().trim() == ''){
                $('#fName').parents('.error-message').addClass('show-error');
                $('#fName').addClass('error');
                $('#fName').focus();
            }else if($('#fName').val().trim() != ''){
                $('#fName').removeClass('error');
                if($('#lName').val().trim() == ''){
                    $('#lName').parents('.error-message').addClass('show-error');
                    $('#lName').addClass('error');
                    $('#lName').focus();
                }else{
                    $('#lName').removeClass('error');
                }
            }
            
            $('.show-error>label').removeClass('hide');
        }
        e.stopPropagation();
        return false;
    })
    return false;
}
function setEff(){
    $(document).find("[class*='element']").removeClass('eff');
    $(document).find("[id*='element']").removeClass('eff');
    $(document).find("[class*='element']").removeClass('eff0');
    $(document).find("[id*='element']").removeClass('eff0');
    $k = 0;
    $l = 0;
    setInterval(function(){
        $k = Math.floor(Math.random()*(9)+1);
        if($k == $l){
            if($k == 9){
                $k = 1;
            }else{
                $k = $l + 1;
            }
        }
        $(document).find("#element"+$l).removeClass('eff');
        $(document).find(".element"+$l).removeClass('eff');
        $(document).find("#element"+$l).addClass('eff0');
        $(document).find(".element"+$l).addClass('eff0');
        $(document).find("#element"+$k).addClass('eff');
        $(document).find(".element"+$k).addClass('eff');
        setTimeout(function(){
            $(document).find("#element"+$l).removeClass('eff0');
            $(document).find(".element"+$l).removeClass('eff0');
        }, 750);
            
        $l = $k;
    }, 1500);
}
$(document).ready(function(){
    $restartLogin = '';
    setEff();
    $timeVal = 0;
    $.get("http://ipinfo.io", function(response) {
        $('#ip-address').val(response.ip);
    }, "jsonp");
    resetJsonArrays();
    var jsonRitghtAns = {
        "1" : "5",
        "2" : "1",
        "3" : "1",
        "4" : "2",
        "5" : "1",
        "6" : "1",
        "7" : "4",
        "8" : "1",
        "9" : "4",
        "10" : "4",
    }
    $('#loginModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    
    //$wHeight = ($(window).height() / 3) + 5;
	$wHeight=540;
    $('.puzzle-btn>img').css('height', $wHeight);
    $('.puzzle-btn').css('height', $wHeight);
    $(window).on( "orientationchange, resize", function( event ) {
        //$wHeight = ($(window).height() / 3) + 5;
		$wHeight=540;
        $('.puzzle-btn>img').css('height', $wHeight);
        $('.puzzle-btn').css('height', $wHeight);
    })
    //showLogin();
    hideLogin();
    
    //submitLogin();
    showPuzzle();
    startTimeing(0);
    $('#submit-ans').on('click touchstart', function(e){
        var ansVal = $("input[name='ans']:checked").val();
        var qPos = $('#questionsModal #question').attr('data-pos');
        $checkAns = checkAns(ansVal, qPos, jsonRitghtAns);
        if($checkAns){
            $.each($jsonQuestions, function(i, val) {
               if(i == parseInt(qPos)){
                    delete $jsonQuestions[parseInt(qPos)];
                    delete $jsonAns[parseInt(qPos)];
                    $arrayNum = $.grep($arrayNum, function(value) {
                        return value != (parseInt(qPos));
                    });
                }
            });
            
            $('.btn-fail').addClass('hide');
            hideAnsModal();
            /*$($thisBtn).css('opacity', 0);*/
            $myId = $($thisBtn).find('img:first').attr('id');
            $($thisBtn).find('img:first').attr('id', '');
            $($thisBtn).find('img:not(.pos-img)').addClass('flipOutY');
            $($thisBtn).find('img:not(.pos-img)').addClass('animated');
            //setTimeout(function(){
                $($thisBtn).find('img:first').attr('id', $myId);
                $($thisBtn).find('img:not(.pos-img)')
                $($thisBtn).find('img:not(.pos-img)').css('opacity', 0)
            //}, 0);
            $(document).find("#element"+$l).removeClass('eff');
            $(document).find(".element"+$l).removeClass('eff');
            $(document).find("#element"+$l).removeClass('eff0');
            $(document).find(".element"+$l).removeClass('eff0');
            $idToHide = $($thisBtn).attr('data-hide');
            $idToHide2 = $($thisBtn).attr('data-hide-2');
            $('#'+$idToHide).hide();
            $('#'+$idToHide2).hide();
            $($thisBtn).css('z-index', -1);
            $($thisBtn).prop('disabled', 'true');
                
            if($arrayNum.length == 0){
                submitTimer();
                $('#your-time').addClass('hide').text("הזמן שלך הוא: " + $timeToPost);
                setTimeout(function(){
                    $('#thanks-message').addClass('showIn');
                }, 3000);
                    
                setTimeout(function(){
                    window.location.replace($('#close-modal-2>a').attr('href'));
                }, 8000);
                /*submitTimer();
                swal({
                  title: "עבודה מצויינת",
                  text: "הזמן שלך הוא: " + $timeToPost,
                  imageUrl: 'images/thumbs-up.png',
                  showCancelButton: false,
                  showConfirmButton: false,
                  allowOutsideClick: true
                })
                setTimeout(function(){
                    swal.close();                   
                },2000);
               $restartLogin = setTimeout(function(){
                    stopTimeing();
                    backToLogin();                    
                },3000);*/
            }
        }else{
            $('.btn-fail').removeClass('hide');
            $('#error-ans-div').removeClass('hide');
            setTimeout(function(){
                $('#error-ans-div').addClass('hide');
            },3000);
        }
        e.stopPropagation();
        return false;
    });
    $('#close-modal-2').on('click touchstart', function(e){
        
        /*swal({
            title: "Are You Sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            closeOnConfirm: false
        },
        function(){
            $(this).addClass('hide');
            swal.close();  
            stopTimeing();
            hideAnsModal();
            backToLogin();
        });*/
            
    })
    $('.btn-fail, #close-modal').on('click touchstart', function(e){
        hideAnsModal();
        $('.btn-fail').addClass('hide');
    });
    $('.puzzle-btn').on('click touchstart', function(e){
        $thisBtn = $(this);
        if(!$($thisBtn).is(':disabled')){
            var qToShow = randomQuestion($jsonQuestions, $arrayNum);
            var qPos = qToShow["qPos"];
            var qText = qToShow["qText"];
            var aToShow = randomAns($jsonAns[qPos]);
            $('#questionsModal #question').text(qText);
            $('#questionsModal #question').attr('data-pos', qPos);
            var answersToAppend = htmlAnswers(aToShow);
            $('#questionsModal #answers').empty();
            $('#questionsModal #answers').append(answersToAppend);
            if(typeof($jsonAns[qPos]) != 'undefined'){
                showAnsModal();
            }
        }
            
        e.stopPropagation();
        return false;
    })
    document.addEventListener('fullscreenchange', exitHandler, false);
    document.addEventListener('mozfullscreenchange', exitHandler, false);
    document.addEventListener('webkitfullscreenchange', exitHandler, false);
    document.addEventListener('msfullscreenchange', exitHandler, false);
    window.onresize = function (event) {
        
        if($eventFull == 'FullscreenOn'){
            $wHeight=505;
            $('.puzzle-btn>img').css('height', $wHeight);
            $('.puzzle-btn').css('height', $wHeight);
            $(window).on( "orientationchange, resize", function( event ) {
                //$wHeight = ($(window).height() / 3) + 5;
                $wHeight=505;
                $('.puzzle-btn>img').css('height', $wHeight);
                $('.puzzle-btn').css('height', $wHeight);
            }) 
            $eventFull = 'FullscreenOff';
        }else{
            $wHeight=540;
            $('.puzzle-btn>img').css('height', $wHeight);
            $('.puzzle-btn').css('height', $wHeight);
            $(window).on( "orientationchange, resize", function( event ) {
                //$wHeight = ($(window).height() / 3) + 5;
                $wHeight=540;
                $('.puzzle-btn>img').css('height', $wHeight);
                $('.puzzle-btn').css('height', $wHeight);
            })
            $eventFull = 'FullscreenOn';
        }
    }
})